LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE work.PIC_pkg.all;

ENTITY dma IS
PORT (
   Reset     : IN    STD_LOGIC;
   Clk       : IN    STD_LOGIC;
   RX_full   : IN    STD_LOGIC;
   RX_empty  : IN    STD_LOGIC;
   ACK_out   : IN    STD_LOGIC;
   TX_RDY    : IN    STD_LOGIC;
   Send_comm : IN    STD_LOGIC;
   DMA_ACK   : IN    STD_LOGIC;
   RCVD_Data : IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
   Valid_D   : OUT   STD_LOGIC;
   Data_Read : OUT   STD_LOGIC;
   Write_en  : OUT   STD_LOGIC;
   OE        : OUT   STD_LOGIC;
   DMA_RQ    : OUT   STD_LOGIC;
   READY     : OUT   STD_LOGIC;
   ContBus   : OUT   STD_LOGIC;
   TX_Data   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
   Address   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
   databus   : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
   data_count: IN    STD_LOGIC_VECTOR(4 DOWNTO 0)
);
END dma;

ARCHITECTURE behavior OF dma IS
   TYPE state IS (Idle, RCV, RCV_STOP, TX1, TX2, TX2W);
   SIGNAL current_state, next_state : state;
   SIGNAL RX_count, RX_count_tmp : INTEGER RANGE 0 TO 3;

BEGIN

-- Combinational process
	PROCESS (current_state, databus, RCVD_Data, Send_comm,
	       	TX_RDY, ACK_out, DMA_ACK, RX_count, data_count)
	BEGIN
		RX_count_tmp <= RX_count;
		READY      <= '1';
		DMA_RQ     <= '0';
		OE         <= '1';
		Data_Read  <= '0';
		Valid_D    <= '1';
		Write_en   <= '0';
		TX_Data    <= (others => '0');
		Address    <= (others => '0');
		databus    <= (others => 'Z');
		next_state <= current_state;
		ContBus    <= '0';

		CASE current_state IS
			WHEN Idle =>
				ContBus    <= '0';
				IF (Send_comm = '1') THEN
					READY      <= '0';
					next_State <= TX1;
				ELSIF (unsigned(data_count) >= 3) THEN
					IF (DMA_ACK = '1') THEN
--						DMA_RQ     <= '1';
--						Data_Read  <= '1';
						next_state <= RCV;
					ELSE
						DMA_RQ <= '1';
						next_state <= Idle;
					END IF;
				ELSE
					next_state <= Idle;
				END IF;

			WHEN RCV =>
			    ContBus   <= '0';
				DMA_RQ    <= '1';
				Data_Read <= '1';
				Write_en  <= '1';
				databus   <= RCVD_Data;
				IF (RX_count = 0)  THEN
				    ContBus   <= '1';
					RX_count_tmp <= RX_count + 1;
					Address <= DMA_RX_BUFFER_MSB;
					next_state <= RCV;
				ELSIF (RX_count = 1)  THEN
				    ContBus   <= '1';
					RX_count_tmp <= RX_count + 1;
					Address <= DMA_RX_BUFFER_MID;
					next_state <= RCV;
				ELSIF (RX_count = 2)  THEN
					ContBus   <= '1';
					RX_count_tmp <= 0;
					Address <= DMA_RX_BUFFER_LSB;
					next_state <= RCV_STOP;
				END IF;

			WHEN RCV_STOP =>
				ContBus   <= '1';
				DMA_RQ    <= '1';
				OE        <= '1';
				Write_en  <= '1';
				Address   <= NEW_INST;
				databus   <= x"FF";
				next_state <= Idle;

			WHEN TX1 =>
			    ContBus   <= '1';
				DMA_RQ    <= '1';
				Valid_D    <= '0';
				READY     <= '0';
			    OE         <= '0';
				Address   <= DMA_TX_BUFFER_MSB;
				IF (ACK_out = '0') THEN
					next_state <= TX2;
				ELSIF (TX_RDY = '1') THEN
					TX_Data    <= databus;
					next_state <= TX1;
				ELSE
					next_state <= TX1;
				END IF;

			WHEN TX2 =>
			    ContBus   <= '1';
				DMA_RQ    <= '1';
				READY     <= '0';
				OE        <= '0';
				Valid_D   <= '0';
				TX_Data   <= databus;
				Address   <= DMA_TX_BUFFER_LSB;
				IF (ACK_out = '0') THEN
					next_state <= TX2W;
				ELSE
					next_state <= TX2;
				END IF;

			WHEN TX2W =>
			    ContBus   <= '1';
				READY     <= '0';
				OE        <= '0';
				Valid_D   <= '1';
				DMA_RQ    <= '1';
				TX_Data   <= databus;
				Address   <= DMA_TX_BUFFER_LSB;
				IF (TX_RDY = '1') THEN
					READY     <= '1';
					next_state <= Idle;
				else
					next_state <= TX2W;
				END IF;

		END CASE;
	END PROCESS;
--Registro de estado
PROCESS (Clk, Reset)
BEGIN
    IF Reset = '0' THEN
        current_state <= Idle;
	RX_count <= 0;
    ELSIF Clk'event and Clk = '1' then
        current_state <= next_state;
	    RX_count <= RX_count_tmp;
    END IF;
END PROCESS;

END behavior;
