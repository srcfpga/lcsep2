from vcd2json import WaveExtractor

path_list = [
        'dmatb/clk',
        'dmatb/reset',
        'dmatb/write_en',
        'dmatb/address[7:0]',
        'dmatb/databus[7:0]',
        'dmatb/dma_rq',
        'dmatb/dma_ack',
        'dmatb/tx_data[7:0]',
        ]

extractor = WaveExtractor('DMA.vcd', 'DMA.json', path_list)
extractor.wave_chunk = 18
extractor.execute()
