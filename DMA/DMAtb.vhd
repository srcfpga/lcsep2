library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- use IEEE.NUMERIC_STD.ALL;
use std.env.finish;

USE work.PIC_pkg.all;

entity DMAtb is
end DMAtb;

architecture testbench of DMAtb is

	SIGNAL Reset     : STD_LOGIC;
	SIGNAL Clk       : STD_LOGIC;
	SIGNAL RCVD_Data : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL RX_full   : STD_LOGIC;
	SIGNAL RX_empty  : STD_LOGIC;
	SIGNAL Data_Read : STD_LOGIC;
	SIGNAL ACK_out   : STD_LOGIC;
	SIGNAL TX_RDY    : STD_LOGIC;
	SIGNAL Valid_D   : STD_LOGIC;
	SIGNAL TX_Data   : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL address   : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL databus   : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL data_count: STD_LOGIC_VECTOR(4 DOWNTO 0);
	SIGNAL Write_en  : STD_LOGIC;
	SIGNAL OE        : STD_LOGIC;
	SIGNAL DMA_RQ    : STD_LOGIC;
	SIGNAL DMA_ACK   : STD_LOGIC;
	SIGNAL Send_comm : STD_LOGIC;
	SIGNAL READY     : STD_LOGIC;
	SIGNAL contbus   : STD_LOGIC;

	component dma
		port (
		   Reset     : IN    STD_LOGIC;
		   Clk       : IN    STD_LOGIC;
		   RX_full   : IN    STD_LOGIC;
		   RX_empty  : IN    STD_LOGIC;
		   ACK_out   : IN    STD_LOGIC;
		   TX_RDY    : IN    STD_LOGIC;
		   DMA_ACK   : IN    STD_LOGIC;
		   Send_comm : IN    STD_LOGIC;
		   RCVD_Data : IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
		   data_count: IN    STD_LOGIC_VECTOR(4 DOWNTO 0);
		   Data_Read : OUT   STD_LOGIC;
		   Write_en  : OUT   STD_LOGIC;
		   OE        : OUT   STD_LOGIC;
		   DMA_RQ    : OUT   STD_LOGIC;
		   Valid_D   : OUT   STD_LOGIC;
		   READY     : OUT   STD_LOGIC;
		   TX_Data   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
		   address   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
		   databus   : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		   contbus   : OUT   STD_LOGIC
	);
	end component;

	constant clk_period : time := 50 ns;
	constant clk_cycle  : time := 2*clk_period;

begin
UUT: dma
    port map (
	 Reset      => Reset,
	 Clk        => Clk,
	 RCVD_Data  => RCVD_Data,
	 RX_full    => RX_full,
	 RX_empty   => RX_empty,
	 Data_Read  => Data_Read,
	 ACK_out    => ACK_out,
         TX_RDY     => TX_RDY,
         Valid_D    => Valid_D,
         TX_Data    => TX_Data,
         address    => address,
         databus    => databus,
         Write_en   => Write_en,
         OE         => OE,
         DMA_RQ     => DMA_RQ,
         DMA_ACK    => DMA_ACK,
         Send_comm  => Send_comm,
         data_count => data_count,
         READY      => READY,
         contbus    => contbus
    );

    -- Reloj
	process
	begin
		Clk <= '1', '0' after clk_period;
		wait for clk_cycle;
	end process;

	-- Reset
	process
	begin
		Reset <= '1', '0' after clk_cycle,
			 '1' after 2*clk_cycle;
		wait;
	end process;

    -- DMA
    process
    begin
	data_count <= "00011";
        -- RX
        RX_full <= '0'; --useless
        RX_empty <= '1' , '0' after 4*clk_cycle,
            '1' after 5*clk_cycle,
            '0' after 10*clk_cycle,
            '1' after 11*clk_cycle,
            '0' after 20*clk_cycle,
            '1' after 21*clk_cycle,
            '0' after 45*clk_cycle,
            '1' after 46*clk_cycle;

        RCVD_Data <= x"10", x"20" after 10*clk_cycle,
            x"30" after 20*clk_cycle,
            x"00" after 30*clk_cycle;

        DMA_ACK <= '0', '1' after 6*clk_cycle,
            '0' after 7*clk_cycle,
            '1' after 13*clk_cycle,
            '0' after 15*clk_cycle,
            '1' after 22*clk_cycle,
            '0' after 23*clk_cycle,
            '1' after 26*clk_cycle,
            '0' after 27*clk_cycle;

        --TX
        TX_RDY <= '0', '1' after 34*clk_cycle,
            '0' after 35*clk_cycle,
            '1' after 40*clk_cycle,
            '0' after 41*clk_cycle;

        Send_comm <= '0', '1' after 32*clk_cycle,
            '0' after 33*clk_cycle,
            '1' after 45*clk_cycle,
            '0' after 46*clk_cycle;

        ACK_out <= '1', '0' after 36*clk_cycle,
            '1' after 37*clk_cycle,
            '0' after 44*clk_cycle,
            '1' after 45*clk_cycle;

        databus <= (others => 'Z'), x"0A" after 30*clk_cycle,
                   x"0B" after 40*clk_cycle;

        wait for 55*clk_cycle;

		report "Calling 'finish'"; finish;

	end process;
end testbench;
