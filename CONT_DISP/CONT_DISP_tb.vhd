----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/12/2021 08:40:10 PM
-- Design Name:
-- Module Name: CONT_DISP_tb - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.env.finish;

USE work.PIC_pkg.all;

entity CONT_DISP_tb is
end CONT_DISP_tb;

architecture testbench of CONT_DISP_tb is

	signal Clk         : std_logic;
	signal Reset       : std_logic;
	signal TEMP_L      : std_logic_vector(6 downto 0);
	signal TEMP_H      : std_logic_vector(6 downto 0);
	signal Display     : std_logic_vector(1 downto 0);
	signal DataDisplay : std_logic_vector(7 downto 0);

	component CONT_DISP
	port (
	      Clk          : in  std_logic;
	      Reset        : in  std_logic;
	      TEMP_L       : in  std_logic_vector(6 DOWNTO 0);
		  TEMP_H       : in  std_logic_vector(6 DOWNTO 0);
		  Display      : out std_logic_vector(1 DOWNTO 0);
		  DataDisplay  : out std_logic_vector(7 DOWNTO 0)
	);
	end component;
	
	constant clk_period : time := 50 ns;
	constant clk_cycle  : time := 2*clk_period;

begin
    UUT: CONT_DISP
        port map (
		 Clk   => Clk,
		 Reset => Reset,
		 TEMP_L => TEMP_L,
		 TEMP_H  => TEMP_H,
		 Display   => Display,
		 DataDisplay    => DataDisplay
        );

    -- Reloj
	process
	begin
		Clk <= '1', '0' after clk_period;
		wait for clk_cycle;
	end process;

	-- Reset
	process
	begin
		Reset <= '1', '0' after clk_cycle,
			 '1' after 2*clk_cycle;
		wait;
	end process;


    -- Entrada de Datos
    process
    begin
	    -- First Test --
	    TEMP_H <= "0000110", "1011011" after 20000*clk_cycle,
	    "0000110" after 40000*clk_cycle,
	    "1011011" after 60000*clk_cycle;
	    
	    TEMP_L <= "0111111", "0000110" after 30000*clk_cycle,
	    "1011011" after 50000*clk_cycle,
	    "1001111" after 70000*clk_cycle;
	    -- wait for 1 ms;
	    
        wait for 100000*clk_cycle;
		report "Calling 'finish'"; finish;
    end process;
end testbench;
