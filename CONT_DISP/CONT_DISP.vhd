----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/10/2021 04:39:20 PM
-- Design Name:
-- Module Name: CONT_DISP - Behavioural
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

USE work.PIC_pkg.all;

ENTITY CONT_DISP IS
	PORT (
		     Clk          : IN  STD_LOGIC;
		     Reset        : IN  STD_LOGIC;
		     TEMP_L       : IN  STD_LOGIC_VECTOR(6 DOWNTO 0);
		     TEMP_H       : IN  STD_LOGIC_VECTOR(6 DOWNTO 0);
		     Display      : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		     DataDisplay  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	     );
END CONT_DISP;
ARCHITECTURE Behavioural OF CONT_DISP IS
	SIGNAL     BitCounter          : INTEGER RANGE 0 TO 20000 := 0;
	SIGNAL     Data_display_temp   : STD_LOGIC_VECTOR (7 downto 0);
	SIGNAL     Display_temp        : STD_LOGIC_VECTOR (1 downto 0);
	SIGNAL     Show                : STD_LOGIC;
	CONSTANT   T1ms                : natural := 20000;
	
BEGIN
    PROCESS (TEMP_H, TEMP_L, Show)
    BEGIN
        IF Show = '0' THEN
            Data_display_temp   <= '1' & TEMP_L;
            Display_temp        <= "00";
        ELSE
            Data_display_temp   <= '1' & TEMP_H; 
            Display_temp        <= "01";    
        END IF;     
    END PROCESS;

	-- Contador
	PROCESS (Clk, Reset)
	BEGIN
		IF Reset = '0' THEN
			Display          <= (others => '0');
			DataDisplay      <= (others => '0');
			BitCounter       <= 0;
			Show             <= '0';
		ELSIF Clk'event AND Clk='1' THEN
		    Display          <= Display_temp;
            DataDisplay      <= Data_Display_temp;
			IF BitCounter < T1ms THEN
			    BitCounter   <= BitCounter + 1;
			ELSE
			    Show         <= not Show; 
				BitCounter   <= 0;
			END IF;
		END IF;
	END PROCESS;
END Behavioural;
