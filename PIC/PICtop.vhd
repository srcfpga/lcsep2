
LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.numeric_std.all;

USE work.PIC_pkg.all;

entity PICtop is
  port (
    Reset    : in  std_logic;           -- Asynchronous, active low
    Clk100MHz: in  std_logic;           -- System clock, 20 MHz, rising_edge
    RS232_RX : in  std_logic;           -- RS232 RX line
    RS232_TX : out std_logic;           -- RS232 TX line
    switches : out std_logic_vector(7 downto 0);   -- Switch status bargraph
    Temp     : out std_logic_vector(7 downto 0);   -- Display value for T_STAT
    Disp     : out std_logic_vector(1 downto 0));  -- Display activation for T_STAT
end PICtop;

architecture behavior of PICtop is

  component CPU
    port(
      Reset     : in    STD_LOGIC;
      Clk       : in    STD_LOGIC;
      ROM_Data  : in    STD_LOGIC_VECTOR (11 downto 0);
      ROM_Addr  : out   STD_LOGIC_VECTOR (11 downto 0);
      RAM_Addr  : out   STD_LOGIC_VECTOR (7  downto 0);
      RAM_Write : out   STD_LOGIC;
      RAM_OE    : out   STD_LOGIC;
      Databus   : inout STD_LOGIC_VECTOR (7  downto 0);
      DMA_RQ    : in    STD_LOGIC;
      DMA_ACK   : out   STD_LOGIC;
      SEND_comm : out   STD_LOGIC;
      DMA_READY : in    STD_LOGIC;
      Alu_op    : out   alu_op;
      Index_Reg : in    STD_LOGIC_VECTOR (7  downto 0);
      FlagZ     : in    STD_LOGIC;
      FlagC     : in    STD_LOGIC;
      FlagN     : in    STD_LOGIC;
      FlagE     : in    STD_LOGIC);
  end component;

--   component Clk_Gen
--     port (
--       reset     : in  std_logic;
--       clk_in1   : in  std_logic;
--       clk_out1  : out std_logic;
--       locked    : out std_logic);
--   end component;

  component RS232top
    port (
      Reset      : in  std_logic;
      Clk        : in  std_logic;
      Data_in    : in  std_logic_vector(7 downto 0);
      Valid_D    : in  std_logic;
      Ack_in     : out std_logic;
      TX_RDY     : out std_logic;
      TD         : out std_logic;
      RD         : in  std_logic;
      Data_out   : out std_logic_vector(7 downto 0);
      Data_read  : in  std_logic;
      Full       : out std_logic;
      Empty      : out std_logic;
      Data_count : out std_logic_vector(4 downto 0)
      );
  end component;

  component DMA
    port(
      Reset     : in    STD_LOGIC;
      Clk       : in    STD_LOGIC;
      RX_full   : in    STD_LOGIC;
      RX_empty  : in    STD_LOGIC;
      ACK_out   : in    STD_LOGIC;
      TX_RDY    : in    STD_LOGIC;
      Send_comm : in    STD_LOGIC;
      DMA_ACK   : in    STD_LOGIC;
      RCVD_Data : in    STD_LOGIC_VECTOR(7 DOWNTO 0);
      Valid_D   : out   STD_LOGIC;
      Data_Read : out   STD_LOGIC;
      Write_en  : out   STD_LOGIC;
      OE        : out   STD_LOGIC;
      DMA_RQ    : out   STD_LOGIC;
      READY     : out   STD_LOGIC;
      ContBus   : out   STD_LOGIC;
      TX_Data   : out   STD_LOGIC_VECTOR(7 DOWNTO 0);
      Address   : out   STD_LOGIC_VECTOR(7 DOWNTO 0);
      databus   : inout STD_LOGIC_VECTOR(7 DOWNTO 0);
      Data_count: in    STD_LOGIC_VECTOR(4 downto 0));
  end component;

  component RAM
    port(
      Clk       : in    STD_LOGIC;
      Reset     : in    STD_LOGIC;
      write_en  : in    STD_LOGIC;
      oe        : in    STD_LOGIC;
      address   : in    STD_LOGIC_VECTOR(7 DOWNTO 0);
      databus   : inout STD_LOGIC_VECTOR(7 DOWNTO 0);
      switches  : out   STD_LOGIC_VECTOR(7 DOWNTO 0);
      Temp_L    : out   STD_LOGIC_VECTOR(6 DOWNTO 0);
      Temp_H    : out   STD_LOGIC_VECTOR(6 DOWNTO 0));
  end component;

  component CONT_DISP
     port(
      Clk         : in  STD_LOGIC;
      Reset       : in  STD_LOGIC;
      TEMP_L      : in  STD_LOGIC_VECTOR(6 DOWNTO 0);
      TEMP_H      : in  STD_LOGIC_VECTOR(6 DOWNTO 0);
      Display     : out STD_LOGIC_VECTOR(1 DOWNTO 0);
      DataDisplay : out STD_LOGIC_VECTOR(7 DOWNTO 0));
  end component;

  component ROM
    port(
      Program_counter  : in  std_logic_vector(11 downto 0);
      Instruction      : out std_logic_vector(11 downto 0));
  end component;

  component ALU
    port(
      Reset         : in    STD_LOGIC;
      Clk           : in    STD_LOGIC;
      opcode        : in    alu_op;
      databus       : inout STD_LOGIC_VECTOR(7 DOWNTO 0);
      Index_Reg     : out   STD_LOGIC_VECTOR(7 DOWNTO 0);
      FlagZ         : out   STD_LOGIC;
      FlagC         : out   STD_LOGIC;
      FlagN         : out   STD_LOGIC;
      FlagE         : out   STD_LOGIC
    );
  end component;


  --SIGNALS
  signal Clk            : std_logic;
  signal reset_p        : std_logic;

  --control DMA
  signal ContBus        : std_logic;
  signal DMA_RQ         : std_logic;
  signal DMA_ACK        : std_logic;
  signal data_count     : STD_LOGIC_VECTOR (4 downto 0);

  --databus
  signal databus        : std_logic_vector(7 downto 0);
  --address
  signal address_DMA    : std_logic_vector(7 downto 0);
  signal OEDMA          : std_logic;
  signal OECPU          : std_logic;
  signal OE             : std_logic;
  --RX
  signal RX_Full        : std_logic;
  signal RX_Empty       : std_logic;
  signal Write_en_DMA   : std_logic;
  signal Valid_D        : std_logic;
  signal RCVD_Data      : std_logic_vector(7 downto 0);
  signal Data_read      : std_logic;
  --TX
  signal Send_comm      : std_logic;
  signal READY          : std_logic;
  signal TX_RDY         : std_logic;
  signal TX_Data        : std_logic_vector(7 downto 0);
  signal ACK            : std_logic;

  --RAM
  signal write_en       : std_logic;
  signal address        : std_logic_vector(7 downto 0);
  --Temperatura
  signal Temp_L         : std_logic_vector(6 downto 0);
  signal Temp_H         : std_logic_vector(6 downto 0);

  --CPU
  signal Write_en_CPU   : std_logic;
  signal address_CPU    : std_logic_vector(7 downto 0);

  --ALU
  signal Alu_opS        : alu_op;
  signal Index_Reg      : std_logic_vector(7 downto 0);
  signal FlagZ          : std_logic;
  signal FlagC          : std_logic;
  signal FlagN          : std_logic;
  signal FlagE          : std_logic;

  --ROM
  signal ROM_Data       : std_logic_vector(11 downto 0);
  signal ROM_Addr       : std_logic_vector(11 downto 0);

begin  -- behavior

  -- OE       <= OEDMA        when (DMA_ACK = '0') AND (Send_comm = '1') else OECPU;
  -- write_en <= Write_en_DMA when (DMA_ACK = '0') AND (Send_comm = '1') else Write_en_CPU;
  -- address  <= address_DMA  when (DMA_ACK = '0') AND (Send_comm = '1') else address_CPU;
  OE       <= OEDMA        when ContBus = '1' else OECPU;
  write_en <= Write_en_DMA when ContBus = '1' else Write_en_CPU;
  address  <= address_DMA  when ContBus = '1' else address_CPU;
  reset_p <= not(Reset);
  Clk <= Clk100MHz;

  CPU_PHY: CPU
    port map(
        Reset       => Reset,
        Clk         => Clk,
        ROM_Data    => ROM_Data,
        ROM_Addr    => ROM_Addr,
        RAM_Addr    => address_CPU,
        RAM_Write   => Write_en_CPU,
        RAM_OE      => OECPU,
        Databus     => databus,
        DMA_RQ      => DMA_RQ,
        DMA_ACK     => DMA_ACK,
        SEND_comm   => Send_comm,
        DMA_READY   => READY,
        Alu_op      => Alu_opS,
        Index_Reg   => Index_Reg,
        FlagZ       => FlagZ,
        FlagC       => FlagC,
        FlagN       => FlagN,
        FlagE       => FlagE
    );

--  Clock_generator : Clk_Gen
--    port map (
--      reset    => reset_p,   -- The reset of the Clock Generator is active high
--      clk_in1  => Clk100MHz,
--      clk_out1 => open,
--      locked   => open);


  RS232_PHY: RS232top
    port map (
        Reset       => Reset,
        Clk         => Clk,
        Data_in     => TX_Data,
        Valid_D     => Valid_D,
        Ack_in      => ACK,
        TX_RDY      => TX_RDY,
        TD          => RS232_TX,
        RD          => RS232_RX,
        Data_out    => RCVD_Data,
        Data_read   => Data_read,
        Full        => RX_Full,
        Empty       => RX_Empty,
        data_count  => data_count);

  RAM_PHY: RAM
    port map (
        Reset       => Reset,
        Clk         => Clk,
        write_en    => write_en,
        oe          => OE,
        address     => address,
        databus     => databus,
        switches    => switches,
        Temp_L      => Temp_L,
        Temp_H      => Temp_H);

  DMA_PHY: DMA
    port map (
        Reset       => Reset,
        Clk         => Clk,
        RX_full     => RX_Full,
        RX_empty    => RX_Empty,
        ACK_out     => ACK,
        TX_RDY      => TX_RDY,
        Send_comm   => Send_comm,
        DMA_ACK     => DMA_ACK,
        RCVD_Data   => RCVD_Data,
        Valid_D     => Valid_D,
        Data_Read   => Data_read,
        Write_en    => Write_en_DMA,
        OE          => OEDMA,
        DMA_RQ      => DMA_RQ,
        READY       => READY,
        TX_Data     => TX_Data,
        Address     => address_DMA,
        databus     => databus,
        ContBus     => ContBus,
        data_count  => data_count);

  CONT_PHY: CONT_DISP
    port map (
        Reset       => Reset,
        Clk         => Clk,
        TEMP_L      => Temp_L,
        TEMP_H      => Temp_H,
        Display     => Disp,
        DataDisplay => Temp);

  ALU_PHY: ALU
    port map(
        Reset       => Reset,
        Clk         => Clk,
        opcode      => Alu_opS,
        databus     => databus,
        Index_Reg   => Index_Reg,
        FlagZ       => FlagZ,
        FlagC       => FlagC,
        FlagN       => FlagN,
        FlagE       => FlagE);

  ROM_PHY: ROM
    port map(
        Instruction      => ROM_Data,
        Program_counter  => ROM_Addr);

end behavior;
