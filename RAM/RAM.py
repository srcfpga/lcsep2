from vcd2json import WaveExtractor

path_list = [
        'ramtb/uut/clk',
        'ramtb/uut/reset',
        'ramtb/uut/write_en',
        'ramtb/uut/oe',
        'ramtb/uut/address[7:0]',
        'ramtb/uut/databus[7:0]',
        'ramtb/uut/switches[7:0]',
        ]

extractor = WaveExtractor('RAM.vcd', 'RAM.json', path_list)
extractor.wave_chunk = 14
extractor.execute()
