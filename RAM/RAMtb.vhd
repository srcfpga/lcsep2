----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name: RAM
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- use IEEE.NUMERIC_STD.ALL;
use std.env.finish;

USE work.PIC_pkg.all;

entity RAMtb is
end RAMtb;

architecture testbench of RAMtb is

	SIGNAL Clk      : std_logic;
	SIGNAL Reset    : std_logic;
	SIGNAL write_en : std_logic;
	SIGNAL oe       : std_logic;
	SIGNAL address  : std_logic_vector(7 downto 0);
	SIGNAL databus  : std_logic_vector(7 downto 0);
	SIGNAL switches : std_logic_vector(7 downto 0);

	component ram
		port (
		     Clk      : IN    std_logic;
		     Reset    : IN    std_logic;
		     write_en : IN    std_logic;
		     oe       : IN    std_logic;
		     address  : IN    std_logic_vector(7 downto 0);
		     databus  : INOUT std_logic_vector(7 downto 0);
		     switches : OUT   std_logic_vector(7 DOWNTO 0)
	);
	end component;

	constant clk_period : time := 50 ns;
	constant clk_cycle  : time := 2*clk_period;

 	procedure write_byte(
		SIGNAL en             : OUT std_logic;
		SIGNAL address_channel: OUT std_logic_vector(7 downto 0);
		CONSTANT addr         : IN  std_logic_vector(7 downto 0);
		SIGNAL data_channel   : OUT std_logic_vector(7 downto 0);
		CONSTANT byte         : IN  std_logic_vector(7 downto 0)
	) is
	begin
		en              <= '1';
		address_channel <= addr;
		data_channel    <= byte;
		wait for clk_cycle;
		en <= '0';
		address_channel <= (others => 'Z');
		data_channel    <= (others => 'Z');
	end procedure write_byte;

 	procedure read_byte(
		signal   en:              OUT std_logic;
		signal   address_channel: OUT std_logic_vector(7 downto 0);
		constant addr:            IN  std_logic_vector(7 downto 0)
	) is
	begin
		en <= '0';
		address_channel <= addr;
		wait for clk_cycle;
		en <= '1';
		address_channel <= (others => 'Z');
	end procedure read_byte;

begin
UUT: ram
        port map (
		 Clk       => Clk,
		 Reset     => Reset   ,
		 write_en  => write_en,
		 oe        => oe,
		 address   => address ,
		 databus   => databus,
		 switches   => switches
        );

        -- Reloj
	process
	begin
		Clk <= '1', '0' after clk_period;
		wait for clk_cycle;
	end process;

	-- Reset
	process
	begin
		Reset <= '1', '0' after clk_cycle,
			 '1' after 2*clk_cycle;
		wait;
	end process;

	-- Send Data
	process
	begin
		write_en <= '0';
		oe <= '1';
		address <= (others => 'Z');
		databus <= (others => 'Z');
		wait for 3*clk_cycle;

		write_byte(write_en, address, x"17", databus, x"01");
		write_byte(write_en, address, x"16", databus, x"01");
		write_byte(write_en, address, x"10", databus, x"01");
		wait for clk_cycle;

		write_byte(write_en, address, DMA_RX_BUFFER_MSB, databus, x"69");
		write_byte(write_en, address, DMA_RX_BUFFER_MID, databus, x"22");
		write_byte(write_en, address, DMA_RX_BUFFER_LSB, databus, x"AA");
		write_byte(write_en, address, NEW_INST         , databus, x"01");
		write_byte(write_en, address, T_STAT           , databus, x"17");
		write_byte(write_en, address, GP_RAM_BASE      , databus, x"aa");
		wait for 4*clk_cycle;

		read_byte(oe, address, x"02");
		read_byte(oe, address, x"03");
		read_byte(oe, address, T_STAT);
		read_byte(oe, address, x"03");
		read_byte(oe, address, GP_RAM_BASE);
		wait for 7*clk_cycle;

		report "Calling 'finish'"; finish;
	end process;

end testbench;
