
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE work.PIC_pkg.all;

ENTITY ram IS
PORT (
   Clk      : IN    STD_LOGIC;
   Reset    : IN    STD_LOGIC;
   write_en : IN    STD_LOGIC;
   oe       : IN    STD_LOGIC;
   address  : IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
   databus  : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
   switches : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
   Temp_L   : OUT   STD_LOGIC_VECTOR(6 DOWNTO 0);
   Temp_H   : OUT   STD_LOGIC_VECTOR(6 DOWNTO 0));
END ram;

ARCHITECTURE behavior OF ram IS

  SIGNAL contents_ram : array8_ram(16#3F# downto 0);
  SIGNAL gp_ram       : array8_ram(16#FF# downto 16#40#);

BEGIN

	PROCESS (Reset, Clk)
	BEGIN
		IF Reset = '0' THEN
			contents_ram <= (OTHERS => (OTHERS => '0'));
			-- Temperatura fija
			-- contents_ram(TO_INTEGER(UNSIGNED(T_STAT))) <= x"aa";
			-- Transmision DMA
			-- contents_ram(TO_INTEGER(UNSIGNED(DMA_TX_BUFFER_MSB))) <= x"bb";
			-- contents_ram(TO_INTEGER(UNSIGNED(DMA_TX_BUFFER_LSB))) <= x"cc";
		ELSIF Clk'event and Clk = '1' THEN
			IF write_en = '1' THEN
				IF UNSIGNED(address) < 16#3f# THEN
					contents_ram(TO_INTEGER(UNSIGNED(address))) <= databus;
				ELSE
					gp_ram(TO_INTEGER(UNSIGNED(address))) <= databus;
				END IF;
			END IF;
		END IF;
	END PROCESS;

	switches_out:
	FOR I IN 7 DOWNTO 0 GENERATE
		switches(I) <= contents_ram(16#10# + I)(0);
	END GENERATE;

	databus <= contents_ram(TO_INTEGER(UNSIGNED(address))) when
		   UNSIGNED(address) < 16#3f# and oe = '0'
		   ELSE gp_ram(TO_INTEGER(UNSIGNED(address))) when
		   oe = '0' ELSE (OTHERS => 'Z');

-------------------------------------------------------------------------

-------------------------------------------------------------------------
-- Decodificador de BCD a 7 segmentos
-------------------------------------------------------------------------
with contents_ram(TO_INTEGER(UNSIGNED(T_STAT)))(7 downto 4) select
Temp_H <=
   "0111111" when "0000",  -- 0
   "0000110" when "0001",  -- 1
   "1011011" when "0010",  -- 2
   "1001111" when "0011",  -- 3
   "1100110" when "0100",  -- 4
   "1101101" when "0101",  -- 5
   "1111101" when "0110",  -- 6
   "0000111" when "0111",  -- 7
   "1111111" when "1000",  -- 8
   "1101111" when "1001",  -- 9
   "1110111" when "1010",  -- A
   "1111100" when "1011",  -- B
   "0111001" when "1100",  -- C
   "1011110" when "1101",  -- D
   "1111001" when "1110",  -- E
   "1110001" when "1111",  -- F
   "0111111" when others;  -- 0
   -- "1111001" when others;  -- E rror

with contents_ram(TO_INTEGER(UNSIGNED(T_STAT)))(3 downto 0) select
Temp_L <=
   "0111111" when "0000",  -- 0
   "0000110" when "0001",  -- 1
   "1011011" when "0010",  -- 2
   "1001111" when "0011",  -- 3
   "1100110" when "0100",  -- 4
   "1101101" when "0101",  -- 5
   "1111101" when "0110",  -- 6
   "0000111" when "0111",  -- 7
   "1111111" when "1000",  -- 8
   "1101111" when "1001",  -- 9
   "1110111" when "1010",  -- A
   "1111100" when "1011",  -- B
   "0111001" when "1100",  -- C
   "1011110" when "1101",  -- D
   "1111001" when "1110",  -- E
   "1110001" when "1111",  -- F
   "0111111" when others;  -- 0
   -- "1111001" when others;  -- E rror
-------------------------------------------------------------------------

END behavior;
