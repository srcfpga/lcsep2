\documentclass{article}
% \documentclass[draft]{article}

\usepackage[spanish]{babel}
\usepackage[a4paper, top = 20mm, bottom=20mm, right=20mm, left=20mm]{geometry}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{rotating}
\usepackage[hidelinks]{hyperref}
\usepackage{tikz}
\usepackage[final]{listings}
\setlength{\parindent}{3mm}
\setlength{\parskip}{5mm}
\linespread{1.65}
% \renewcommand{\figurename}{Fig.}

\title{\textsc{Implementación de un microcontrolador\\
de propósito específico en FPGA}
\thanks{Master en Ingeniería de Sistemas
Electrónicos, Escuela Técnica Superior de Ingenieros de Telecomunicación,
Madrid, España.}\\ \vspace{\baselineskip}
\textbf{-- RS232 -- DMA -- RAM -- ROM -- ALU -- CPU -- }
\\ \vspace{\baselineskip}
\date{\today}
\author{
\href{mailto:ignacio.amat@upm.es}{Ignacio Amat}
\hspace{2cm}
\href{mailto:alejandro.martinez@upm.es}{Alejandro Martinez}
}
}

\lstdefinestyle{mystyle}{
	language=VHDL,
	commentstyle=\color{red},
	keywordstyle=\color{blue},
	morekeywords={STD_LOGIC, INTEGER, STD_LOGIC_VECTOR},
	numberstyle=\color{gray},
	basicstyle=\linespread{.8}\ttfamily\footnotesize,
	breakatwhitespace=false,
	breaklines=true,
	captionpos=b,
	keepspaces=true,
	numbers=left,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	frame=single
}

\lstset{style=mystyle}
\begin{document}

\maketitle
\clearpage
\tableofcontents
\listoffigures
\clearpage

\section{Introducción}

En esta práctica se implementa en FPGA mediante VHDL un microcontrolador de
propósito específico. El diagrama de alto nivel se muestra a continuación:
\vspace{.5\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics{sistema.pdf}
	\caption{Esquema del sistema.}
\end{figure}
\vspace{-.5\baselineskip}

El microcontrolador se controla externamente mediante cuatro comandos, I,  A,  T
o S; el primero apaga o enciendo uno  de  los  ocho  interruptores;  el  segundo
asigna un valor de apertura a uno de los 10 actuadores; el  tercero  escribe  un
valor de temperatura; el último solicita  información	sobre  los  periféricos.

Los comandos son  recibidos  a	través	de  la	línea  RD  desde  el  módulo  de
comunicaciones en serie RS232, se reciben codificados en ASCII.	 El  controlador
de acceso directo a memoria escribe los datos recibidos en el la memoria  usando
el bus de datos.  Dado que el bus de datos  es	común  a  varios  módulos,  para
evitar colisiones, el acceso a él está regido por la CPU.  El  DMA  solicita  el
bus al procesador cuando necesita transferir datos.  Además  del  bus  de  datos
compartido, el sistema cuenta con un bus de instrucciones entre la  memoria  ROM
y el procesador.  La memoria ROM almacena el programa a ejecutar, el  procesador
va leyendo las	instrucciones  y  realizando  las  acciones  programadas.   Esta
arquitectura, con memoria independiente para datos e instrucciones  se	denomina
\textit{Arquitectura de Harvard}, contrasta con la \textit{Arquitectura  de  von
Neumann}, que usa un único espacio de memoria.

A continuación se detallan los módulos implementados.

\clearpage

\section{RS232-TX}
\subsection{Comportamiento}

El comportamiento del módulo de transmisión se muestra en la siguiente figura.
A grandes rasgos, el módulo serializa el byte que entra por \texttt{data[7:0]}
y lo envia por la línea \texttt{TX}. Mientras se retransmiten datos se mantiene
la señal \texttt{eot} a nivel bajo. El diagrama de tiempo esperado sería
similar al siguiente.
\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{RS232-TX.tex}
	\caption{Diagrama de tiempo RS232-TX.}
	\label{fig:tx}
\end{figure}

En el diagrama de tiempo \hyperref[fig:tx]{\textbf{Fig.}~\ref*{fig:tx}} se ve
como primero entra el \texttt{Reset} asíncrono. Después se escribe el dato de
entrada en \texttt{data[7:0]} (\texttt{0xAF}, en el primer caso), seguidamente
se pone a HIGH la señal de \texttt{Start}, que normalmente está LOW. Cuando
comienza la transmisión, el módulo TX va leyendo \texttt{data[7:0]} y
transmitiendo en la línea de trasmisión (\texttt{TX}) cada uno de los bits en
serie. Mientras el módulo TX está transmitiendo datos la línea \texttt{EOT}
baja desde HIGH y se mantiene en LOW cuanto dure la transmisión. Además de
enviar los 8 bits que componen el dato, el módulo TX envía un primer bit de
comienzo LOW y un último bit de parada HIGH.

\subsection{Implementación}
\lstinputlisting[
	firstline = 36,
	label = lst:tx,
	caption = {RS232-TX.vhd}
]{RS232-TX.vhd}

El código se muestra en el  \hyperref[lst:tx]{\textbf{Lst.}~\ref*{lst:tx}}.   Se
ha implementado mediante dos procesos, uno asíncrono que describe la  lógica  de
estado siguiente y otro proceso síncrono que se  encarga  del  \texttt{reset}  y
mantiene  las  variables  internas  registradas.   Las	salidas  \texttt{TX}   y
\texttt{EOT}   se   actualizan	 antes	 cambios   en	la   variable	 interna
\texttt{current\_state}. Hemos definido dos contadores internos,
\texttt{baud\_cnt} y \texttt{data\_cnt} que lleva la cuenta del bit  de  entrada
leído, cuenta desde 0 hasta 7.	\texttt{baud\_cnt}  mantiene  la  sincronía  del
sistema con los baudios a los que se quiere que se transmita la  salida.   Hemos
definimos  otra  señal	interna,  \texttt{current\_state}   que   toma	 valores
\texttt{Idle}, \texttt{StartBit},  \texttt{Send}  y  \texttt{Stop},  los  cuatro
posibles estados definidos.

\clearpage
\subsection{Simulación}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .87\linewidth]{./RS232-TX.png}
	\caption{Simulación RS232-TX.}
\end{figure}

En la simulación con el banco de pruebas suministrado vemos el comportamiento
esperado en el módulo TX. Adicionalmente se muestran los cambios en la variable
\texttt{current\_state} y el valor de \texttt{data\_count}.

\begin{figure}[h!]
	\centering
	\includegraphics[width = .87\linewidth]{./RS232-TX_psts.png}
	\caption{Simulación RS232-TX post-síntesis.}
\end{figure}


\clearpage

\section{RS232-RX}
\subsection{Comportamiento}

La lógica desarrollada en el módulo de recepción es casi idéntica a la
desarrollada anteriormente. Este módulo toma el dato en serie de la señal de
recepción \texttt{LineRD\_in} (anteriormente señal de transmisión \texttt{TX})
y lo redirige mediante la línea \texttt{Code\_Out} hacia el \textit{Shift
Register}.  Mientras el módulo de recepción está enviando datos al
\textit{Shift Register}, mantiene la señal \texttt{Valid\_Out} en LOW
(normalmente está en HIGH). El módulo de recepción dispara su máquina de
estados cuando la línea \texttt{Line\_in} pasa a LOW, el \texttt{StartBit}, que
indica que los siguiente bits en serie se corresponden con los datos.
Lógicamente el \texttt{StartBit} no es transmitido al \textit{Shift Register}
para ser almacenado. El código implementado se muestra en el
\hyperref[lst:tx]{\textbf{Lst.}~\ref*{lst:rx}}.

\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{RS232-RX.tex}
	\caption{Diagrama de tiempo RS232-RX.}
\end{figure}

\clearpage

\subsection{Implementación}
\vspace{\baselineskip}
\lstinputlisting[
	firstline = 37,
	label = lst:rx,
	caption = {RS232-RX.vhd}
]{RS232-RX.vhd}

\vspace{-.5\baselineskip}

La implementación es muy similar a la realizada para el módulo	de  transmisión.
Una aspecto particular de la recepción es cuando  se  realiza  el  muestreo  del
dato  de  entrada,  para   realizar   esta   tarea   definimos	 una   constante
\texttt{sample}.  Cuando la modificamos vemos como  el	pico  de  la  señal  que
valida la salida del módulo \texttt{Valid\_out} se desplaza a la derecha y a  la
izquierda,   indicando	 cuando   se   realiza	 la   lectura	en   el   estado
\texttt{Receive}.


\clearpage
\subsection{Simulación}
% \vspace{\baselineskip}

\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./RS232-RX.png}
	\caption{Simulación RS232-RX.}
\end{figure}

En la simulación observamos el comportamiento esperado, se muestran las
variables internas. En los picos de \texttt{Valid\_out} se aprecia el muestro
de la señal al cincuenta por ciento del bit. Cuando alcanzamos el estado
\texttt{StopBit} mandamos un pulso por en \texttt{Store\_out} a la memoria
interna. En la siguiente figura se muestra como variaría nuestro sistema cuando
modificamos el tiempo de muestreo, primero lo adelantamos y leemos el bit antes
y después lo restrasamos, leyendo el bit casi al final.

\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./RS232-RX_psts.png}
	\caption{Simulación RS232-RX post-síntesis.}
\end{figure}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./RS232-RX_antes.png}
\end{figure}
\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./RS232-RX_desp.png}
	\caption{Simulación RS232-RX modificando tiempos de muestreo.}
\end{figure}
\clearpage

\section{Shift Register}
\subsection{Comportamiento}

El comportamiento del \textit{Shift Register} es el siguiente,
va acumulando los bits en serie que llegan por \texttt{D} en una
variable interna \texttt{Q\_tmp}. Cuando se termina la transmisión
se escribe el byte en \texttt{Q[7:0]}.
\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\def\svgwidth{.9\textwidth}
	\input{ShiftRegister.tex}
	\caption{Diagrama de tiempos de Shift Register.}
\end{figure}
\clearpage
\subsection{Implementación}
\vspace{\baselineskip}
\lstinputlisting[
	firstline = 22,
	label = lst:shift,
	caption = {ShiftRegister.vhd}
]{ShiftRegister.vhd}
\vspace{-\baselineskip}
\subsection{Simulación}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./shift.png}
	\caption{Simulación \textit{Shift Register}.}
\end{figure}

\clearpage

\section{RS232}

Una vez implementados y analizados los módulos por separados procedemos
a juntarlos en una prueba final. Usando el \textit{testbench} proporcionado
vemos como se genera  unaseñal en \texttt{TX}  y como se recibe en \texttt{RX}
para posteriormente ser acumulada en el \textit{Shift Register} y después
guardada en una memoria interna FIFO.


\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{rs232.png}
	\vspace{-1.6\baselineskip}
	\caption{Simulación del sistema completo.}
\end{figure}

\clearpage

\phantom{}\vspace{2\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{rs232_psts.png}
	\caption{Simulación del sistema completo post-síntesis.}
\end{figure}

\clearpage

\begin{figure}[h!]
	\centering
	\includegraphics[width = 1.4\linewidth, angle=90]{./rs232.pdf}
	\caption{Esquema del RS232.}
\end{figure}

\section{RAM}
\subsection{Comportamiento}
\vspace{\baselineskip}
\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{RAM.tex}
	\caption{Diagrama de tiempo RAM.}
	\label{fig:ram}
\end{figure}

El comportamiento es el estándar del esperado de una  memoria  RMA.   Cuando  la
señal de \texttt{write\_en} está en nivel alto el valor del \texttt{databus}  es
escrito  en  la  dirección  \texttt{address}.	De  manera   recíproca,   cuando
\texttt{oe} es	puesto	a  nivel  bajo	el  módulo  de	memoria  escribe  en  el
\texttt{databus}   el	valor	guardado   en	la   dirección	 indicada    por
\texttt{adddress}.

\subsection{Implementación}

Para la implementación de la memoria RAM con, las prestaciones	pedidas,  se  ha
optado por dos bancos de memoria.  El primero, de 64 bytes, lo ponemos a `0' con
la llegada del \texttt{reset} asíncrono.  Para comprobar el  funcionamiento  del
sistema, codificamos manualmente el valor de la temperatura (\texttt{x"21"})  en
la dirección por defecto \texttt{T\_STAT}.  Hacemos lo mismo con los dos  bytes,
\texttt{DMA\_TX\_BUFFER\_LSB}, \texttt{DMA\_TX\_BUFFER\_MSB}, el controlador  de
memoria los retransmitirá por la línea TX.

Dentro del proceso síncrono de	nuestra  implementación,  cuando  lo  indica  la
señal \texttt{write\_en}, tomamos el valor del \texttt{databus} y lo  escribimos
en la dirección que marca \texttt{adddress}.  En la  escritura	atendemos  a  la
valor de la dirección para escribir  el  dato  o  en  el  banco  de  memoria  de
propósito general (\texttt{gp\_ram})  o  en  en  las  direcciones  de  propósito
específico (\texttt{contents\_ram}).

En   la   implementación,   combinacionalmente,   sacamos    por    la	  salida
\texttt{switches\_out} los valores de la dirección de  memoria	correspondiente.
Para ello empleamos un \texttt{for-generate}.  De manera similar el bus de datos
se escribe de manera asíncrona según  se  actualice  la  dirección  de	memoria.
Cuando	no  estamos  escribiendo  en  el  \texttt{databus},  se  deja  en   alta
impedancia.

El  último  elemento  de  este	módulo	de  memoria  concierne	a  dos	 salidas
especializadas,  los  bytes  correspondientes  a  la  temperatura   a	mostrar,
\texttt{Temp\_H} y \texttt{Temp\_L}.  De  manera  combinacional,  traducimos  el
valor guardado en la dirección de memoria \texttt{T\_STAT} al formato  apropiado
para el  display  de  siete  segmentos.   Realizamos  la  misma  operación  para
\texttt{Temp\_H} y \texttt{Temp\_L} dado que tenemos que mostrar la  temperatura
en dos displays distintos.

\vspace{2\baselineskip}
\lstinputlisting[
	firstline = 21,
	label = lst:ram,
	caption = {RAM.vhd}
]{../RAM/RAM.vhd}

\subsection{Simulación}
\vspace{.5\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./ram1.png}
	\caption{Simulación RAM.}
\end{figure}

En la  simulación  se  aprecia	que,  conforme	escribimos  en	las  direcciones
apropiadas, los interruptores correspondientes se actualizan en el  mismo  ciclo
de reloj.

\clearpage

\phantom{}
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./ram2.png}
	\caption{Simulación RAM posiciones de memoria.}
\end{figure}

En este diagrama se ve	como,  cuando  baja  la  señal	de  \textsc{reset},  las
posiciones de memoria se ponen a 0.  También podemos ver los valores almacenados
en las distintas direcciones.

\clearpage

\section{DMA}
\subsection{Comportamiento}

El acceso directo a memoria se ha  implementado  de  acuerdo  con  la  siguiente
máquina de estados.  Funcionalmente  consta  de  dos  partes,  una  controla  la
recepción y otra el envío.  La lógica  de  estado  siguiente  se  rige	por  las
señales que recibe el módulo.  La recepción de los tres datos arranca cuando  la
memoria \textit{FIFO} indica que tiene por  lo	menos  tres  bytes  disponibles.
Este  modulo  también  es  responsable	de  generar  una   señal   de	control,
\textsc{Contbus}, según el estado en el que se encuentre permite o  no	a  otros
módulos utilizar el bus de datos y memoria.

\vspace{3\baselineskip}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./DMA_STATES.jpg}
	\caption{Máquina de estados DMA.}
\end{figure}

\clearpage
% \phantom{}\vspace{2\baselineskip}

Este es el comportamiento esperado del módulo.
% \vspace{\baselineskip}

\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{DMA.tex}
	\caption{Diagrama de tiempo DMA.}
	\label{fig:dma}
\end{figure}

\vspace{2\baselineskip}
\subsection{Implementación}

La implementación se ha realizado usando dos procesos, uno secuencial y uno
combinacional. Se ha decidido usar un contador para la recepción, toma valores
entre 0 y 2 según se va iterando entre los bits de significancia mayor, media y
menor. Para el envío se usan tres estados.
\vspace{\baselineskip}
\lstinputlisting[
	firstline = 32,
	label = lst:dma,
	caption = {DMA.vhd}
]{../DMA/DMA.vhd}

\vspace{-.5\baselineskip}
\subsection{Simulación}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./dma1.png}
	\caption{Simulación DMA Recepción.}
\end{figure}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./dmatop.png}
	\caption{Simulación DMA Recepción con escritura en RAM.}
\end{figure}
\vspace{1.5\baselineskip}
\begin{figure}[h!]
	\centering
	\includegraphics[width = .9\linewidth]{./dma2.png}
	\caption{Simulación DMA Transmisión.}
\end{figure}


\clearpage

\begin{figure}[h!]
	\centering
	\includegraphics[width = 1.4\linewidth, angle = 90]{./dmaschematic.pdf}
	\caption{Esquema del DMA.}
\end{figure}

\section{ALU}
\subsection{Comportamiento}

Este módulo recibe las instrucciones de la CPU por la línea \texttt{opcode}
y opera sobre los datos leídos en el \texttt{databus}. Cuenta con tres registros
internos, \texttt{A, B} y el acumulador \texttt{ACC}.

% \clearpage
\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{ALUdoc.tex}
	\caption{Diagrama de tiempo ALU.}
	\label{fig:alu}
\end{figure}

\clearpage
\subsection{Implementación}
\vspace{2\baselineskip}
\lstinputlisting[
	firstline = 21,
	label = lst:alu,
	tabsize = 1,
	caption = {ALU.vhd}
]{../ALU/ALU.vhd}

\clearpage
\subsection{Simulación}
\vspace{-\baselineskip}
Para la simulación hemos implementado un banco de pruebas que sistemáticamente
y todos los posibles comportamientos. Automáticamente comprueba el correcto
funcionamiento usando \texttt{asserts}.

\begin{figure}[h!]
	\centering
	\includegraphics[width = .8\linewidth]{./alu1.png}
	\includegraphics[width = .8\linewidth]{./alu2.png}
	\includegraphics[width = .8\linewidth]{./alu3.png}
	\caption{Simulación ALU.}
\end{figure}

\clearpage

\begin{figure}[h!]
	\centering
	\includegraphics[width = 1.4\linewidth, angle=90]{./aluschematic.pdf}
	\caption{Esquema del ALU}
\end{figure}

\clearpage

\section{Sistema Completo}

La implementación del sistema completo la realizamos juntando
los módulos y resolviendo los conflictos entre las señales con
un \texttt{MUX}.

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./comp1.png}
	\caption{Sistema Completo}
\end{figure}

\clearpage
\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./comp2.png}
	\caption{Sistema Completo}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width = 1.45\linewidth, angle = 90]{./todoschematic.pdf}
	\caption{Esquema del microcontrolador.}
\end{figure}

\clearpage
\subsection{Utilización}

\begin{figure}[h!]
	\centering
	\includegraphics[width = \linewidth]{./luts.png}
	\caption{Recursos FPGA ocupados}
\end{figure}
\end{document}
