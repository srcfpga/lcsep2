\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{5}{section.1}%
\contentsline {section}{\numberline {2}RS232-TX}{6}{section.2}%
\contentsline {subsection}{\numberline {2.1}Comportamiento}{6}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Implementación}{7}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Simulación}{8}{subsection.2.3}%
\contentsline {section}{\numberline {3}RS232-RX}{9}{section.3}%
\contentsline {subsection}{\numberline {3.1}Comportamiento}{9}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Implementación}{10}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Simulación}{11}{subsection.3.3}%
\contentsline {section}{\numberline {4}Shift Register}{13}{section.4}%
\contentsline {subsection}{\numberline {4.1}Comportamiento}{13}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Implementación}{14}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Simulación}{14}{subsection.4.3}%
\contentsline {section}{\numberline {5}RS232}{15}{section.5}%
\contentsline {section}{\numberline {6}RAM}{18}{section.6}%
\contentsline {subsection}{\numberline {6.1}Comportamiento}{18}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Implementación}{18}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Simulación}{20}{subsection.6.3}%
\contentsline {section}{\numberline {7}DMA}{22}{section.7}%
\contentsline {subsection}{\numberline {7.1}Comportamiento}{22}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Implementación}{23}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Simulación}{25}{subsection.7.3}%
\contentsline {section}{\numberline {8}ALU}{28}{section.8}%
\contentsline {subsection}{\numberline {8.1}Comportamiento}{28}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Implementación}{29}{subsection.8.2}%
\contentsline {subsection}{\numberline {8.3}Simulación}{30}{subsection.8.3}%
\contentsline {section}{\numberline {9}Sistema Completo}{32}{section.9}%
\contentsline {subsection}{\numberline {9.1}Utilización}{35}{subsection.9.1}%
