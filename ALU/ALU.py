from vcd2json import WaveExtractor

path_list = [
        'alutb/clk',
        'alutb/reset',
        'alutb/op',
        'alutb/databus[7:0]',
        'alutb/index_reg[7:0]',
        'alutb/flagz',
        #  'alutb/flagc',
        #  'alutb/flagn',
        #  'alutb/flage',
        ]

extractor = WaveExtractor('ALU.vcd', 'ALU.json', path_list)
extractor.wave_format('alutb/op', 'd')
extractor.wave_chunk = 16
extractor.execute()
