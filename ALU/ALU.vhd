LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE work.PIC_pkg.all;


ENTITY alu IS
PORT (
   Reset     : IN    STD_LOGIC;                    -- active low
   Clk       : IN    STD_LOGIC;                    -- 20 MHz
   opcode    : IN    alu_op;                       -- Instruction from CPU
   databus   : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- High Z when not in use
   Index_Reg : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0); -- Index register
   FlagZ     : OUT   STD_LOGIC;                    -- Zero Flag
   FlagC     : OUT   STD_LOGIC;                    -- Carry Flag
   FlagN     : OUT   STD_LOGIC;                    -- Nibble Carry Bit
   FlagE     : OUT   STD_LOGIC                     -- Error Flag
);
END alu;
ARCHITECTURE behavior OF alu IS
		SIGNAL A, B, ACC : STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
		PROCESS(Clk, Reset)
		BEGIN
				IF (Reset = '0') then
						A         <= (others => '0');
						B         <= (others => '0');
						ACC       <= (others => '1');
						Index_Reg <= (others => '0');
						databus   <= (others => 'Z');
						FlagZ     <= '0';
						FlagC     <= '0';
						FlagN     <= '0';
						FlagE     <= '0';
				ELSIF (CLK'event and CLK='1') THEN
						databus <= (others => 'Z');
						FlagZ   <= '0';
						FlagC   <= '0';
						FlagN   <= '0';
						FlagE   <= '0';
						-- ACC     <= (others => '1');
						CASE opcode IS
								WHEN nop          =>
								WHEN op_lda       => A   <= databus;
								WHEN op_ldb       => B   <= databus;
								WHEN op_ldacc     => ACC <= databus;
								WHEN op_ldid      => Index_Reg <= databus;
								WHEN op_mvacc2id  => Index_Reg <= ACC;
								WHEN op_mvacc2a   => A   <= ACC;
								WHEN op_mvacc2b   => B   <= ACC;
								WHEN op_add       =>
										ACC   <= std_logic_vector(unsigned(A) + unsigned(B));
										FlagZ <= '1' WHEN (unsigned(A) + unsigned(B)) = 0 ELSE '0';
										FlagC <= '1' WHEN (resize(unsigned(A),9) + resize(unsigned(B),9)) > x"ff" ELSE '0';
										FlagN <= '1' WHEN (unsigned(A) + unsigned(B)) > x"0f" ELSE '0';
								WHEN op_sub       =>
										ACC   <= std_logic_vector(unsigned(A) - unsigned(B));
										FlagZ <= '1' WHEN (unsigned(A) - unsigned(B)) = 0 ELSE '0';
										FlagC <= '1' WHEN (resize(unsigned(A),9) - resize(unsigned(B),9)) > x"ff" ELSE '0';
										FlagN <= '1' WHEN (unsigned(A) - unsigned(B)) > x"0f" ELSE '0';
								WHEN op_shiftl    => ACC <= std_logic_vector(unsigned(ACC) sll 1);
								WHEN op_shiftr    => ACC <= std_logic_vector(unsigned(ACC) srl 1);
								WHEN op_and       =>
										ACC   <= std_logic_vector(A and B);
										FlagZ <= '1' WHEN (A and B) = x"00" ELSE '0';
								WHEN op_or        =>
										ACC <= std_logic_vector(A or B);
										FlagZ <= '1' WHEN (A or B) = x"00" ELSE '0';
								WHEN op_xor       =>
										ACC   <= std_logic_vector(A xor B);
										FlagZ <= '1' WHEN (A xor B) = x"00" ELSE '0';
								WHEN op_cmpe      => FlagZ <= '1' when unsigned(A) = unsigned(B) else '0';
								WHEN op_cmpl      => FlagZ <= '1' when unsigned(A) < unsigned(B) else '0';
								WHEN op_cmpg      => FlagZ <= '1' when unsigned(A) > unsigned(B) else '0';
								WHEN op_ascii2bin =>
										ACC   <= std_logic_vector(unsigned(A) - x"30") when ((unsigned(A) >= x"30") and (unsigned(A) <= x"39")) else x"ff";
										FlagE <= '0' when ((unsigned(A) >= x"30") and (unsigned(A) <= x"39")) else '1';
								WHEN op_bin2ascii =>
										ACC   <= std_logic_vector(unsigned(A) + 48) when unsigned(A) >= 0 and unsigned(A) <= 9 else x"ff";
										FlagE <= '0' when unsigned(A) >= 0 and unsigned(A) <= 9 else '1';
								WHEN op_oeacc     => -- databus <= ACC;
						END CASE;
				END IF;
		END PROCESS;
		databus <= ACC when opcode = op_oeacc else (others => 'Z');
END behavior;
