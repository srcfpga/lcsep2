from vcd2json import WaveExtractor

path_list = [
        'aludoctb/clk',
        'aludoctb/reset',
        'aludoctb/op',
        'aludoctb/databus[7:0]',
        'aludoctb/index_reg[7:0]',
        'aludoctb/flagz',
        'aludoctb/flagc',
        'aludoctb/flagn',
        'aludoctb/flage',
        ]

extractor = WaveExtractor('ALUdoc.vcd', 'ALUdoc.json', path_list)
extractor.wave_format('aludoctb/op', 'd')
extractor.wave_chunk = 17
extractor.execute()
