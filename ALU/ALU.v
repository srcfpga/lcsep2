// File DMA.vhd translated with vhd2vl v3.0 VHDL to Verilog RTL translator
// vhd2vl settings:
//  * Verilog Module Declaration Style: 2001

// vhd2vl is Free (libre) Software:
//   Copyright (C) 2001 Vincenzo Liguori - Ocean Logic Pty Ltd
//     http://www.ocean-logic.com
//   Modifications Copyright (C) 2006 Mark Gonzales - PMC Sierra Inc
//   Modifications (C) 2010 Shankar Giri
//   Modifications Copyright (C) 2002-2017 Larry Doolittle
//     http://doolittle.icarus.com/~larry/vhd2vl/
//   Modifications (C) 2017 Rodrigo A. Melo
//
//   vhd2vl comes with ABSOLUTELY NO WARRANTY.  Always check the resulting
//   Verilog for correctness, ideally with a formal verification tool.
//
//   You are welcome to redistribute vhd2vl under certain conditions.
//   See the license (GPLv2) file included with the source for details.

// The result of translation follows.  Its copyright status should be
// considered unchanged from the original VHDL.

// no timescale needed

module alu(
input wire Reset,
input wire Clk,
input wire [7:0] RCVD_Data,
input wire RX_full,
input wire RX_empty,
output wire Data_Read,
input wire ACK_out,
input wire TX_RDY,
output wire Valid_D,
input wire [7:0] TX_Data,
input wire [7:0] address,
inout reg [7:0] databus,
output wire Write_en,
output wire OE,
output wire DMA_RQ,
input wire DMA_ACK,
input wire Send_comm,
output wire READY
);





  always @(posedge Clk, posedge Reset) begin
    if(Reset == 1'b0) begin
    end else begin
      if((DMA_ACK == 1'b1)) begin
        databus <= TX_Data;
      end
      else begin
        databus <= {8{1'bZ}};
      end
    end
  end


endmodule
