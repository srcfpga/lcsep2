library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.env.finish;

USE work.PIC_pkg.all;

entity ALUtb is
end ALUtb;

architecture testbench of ALUtb is

	SIGNAL Reset     : STD_LOGIC;
	SIGNAL Clk       : STD_LOGIC;
	SIGNAL opcode    : alu_op;
	SIGNAL databus   : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL Index_Reg : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL FlagZ     : STD_LOGIC;
	SIGNAL FlagC     : STD_LOGIC;
	SIGNAL FlagN     : STD_LOGIC;
	SIGNAL FlagE     : STD_LOGIC;

	component alu
		port (
		     Reset     : IN    STD_LOGIC;
		     Clk       : IN    STD_LOGIC;
		     opcode    : IN    alu_op;
		     databus   : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		     Index_Reg : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
		     FlagZ     : OUT   STD_LOGIC;
		     FlagC     : OUT   STD_LOGIC;
		     FlagN     : OUT   STD_LOGIC;
		     FlagE     : OUT   STD_LOGIC
		     );
	end component;

	constant clk_period     : time    := 25 ns;
	constant clk_cycle      : time    := 2*clk_period;
	signal op: integer range 0 to 63;

begin
UUT: alu
        port map (
		 Reset     => Reset,
		 Clk       => Clk,
		 opcode    => opcode,
		 databus   => databus,
		 Index_Reg => Index_Reg,
		 FlagZ     => FlagZ,
		 FlagC     => FlagC,
                 FlagN     => FlagN,
                 FlagE     => FlagE
        );

	op <= alu_op'POS(opcode);
	-- Reloj
	process
	begin
		Clk <= '1', '0' after clk_period;
		wait for clk_cycle;
	end process;

	-- Reset
	process
	begin
		Reset <= '1', '0' after clk_cycle,
			 '1' after 2*clk_cycle;
		wait;
	end process;

	-- Send Data
	process
		procedure send_alu(
			CONSTANT code    : IN alu_op;
			CONSTANT byteA   : IN std_logic_vector(7 downto 0);
			CONSTANT byteB   : IN std_logic_vector(7 downto 0);
			CONSTANT Z, C, N : IN std_logic
		) is
		begin
			-- load A
			databus <= byteA;
			opcode  <= op_lda;

			-- load B
			wait for clk_cycle;
			databus <= byteB;
			opcode  <= op_ldb;

			-- operation
			wait for clk_cycle;
			opcode  <= code;
			databus <= (others => 'Z');

			-- result
			wait for clk_cycle;
			opcode  <= op_oeacc;

			assert FlagZ = Z   report "zero   failure" severity failure;
			assert FlagC = C   report "carry  failure" severity failure;
			assert FlagN = N   report "nibble failure" severity failure;
			assert FlagE = '0' report "error  failure" severity failure;

			wait for clk_cycle;
			opcode  <= nop;
		end procedure send_alu;

		procedure send_alu(
			CONSTANT code  : IN alu_op;
			CONSTANT byteA : IN std_logic_vector(7 downto 0);
			CONSTANT byteB : IN std_logic_vector(7 downto 0);
			CONSTANT Z     : IN std_logic
		) is
		begin
			-- load A
			databus <= byteA;
			opcode  <= op_lda;

			-- load B
			wait for clk_cycle;
			databus <= byteB;
			opcode  <= op_ldb;

			-- operation
			wait for clk_cycle;
			opcode  <= code;
			databus <= (others => 'Z');

			assert FlagZ = Z   report "zero   failure" severity failure;
			assert FlagC = '0' report "carry  failure" severity failure;
			assert FlagN = '0' report "nibble failure" severity failure;
			assert FlagE = '0' report "error  failure" severity failure;

			wait for clk_cycle;
			opcode  <= nop;
		end procedure send_alu;

		procedure send_alu(
			CONSTANT code    : IN alu_op;
			CONSTANT byteACC : IN std_logic_vector(7 downto 0);
			CONSTANT E       : IN std_logic
		) is
		begin
			-- load ACC
			databus <= byteACC;
			opcode  <= op_ldacc;

			-- operation
			wait for clk_cycle;
			opcode  <= code;
			databus <= (others => 'Z');

			-- result
			wait for clk_cycle;
			opcode  <= op_oeacc;

			assert FlagE = E report "error  failure" severity failure;

			wait for clk_cycle;
			opcode  <= nop;
		end procedure send_alu;

		procedure send_alu(
			CONSTANT code    : IN alu_op;
			CONSTANT byteA   : IN std_logic_vector(7 downto 0);
			CONSTANT E, Z    : IN std_logic
		) is
		begin
			-- load ACC
			databus <= byteA;
			opcode  <= op_lda;

			-- operation
			wait for clk_cycle;
			opcode  <= code;
			databus <= (others => 'Z');

			-- result
			wait for clk_cycle;
			opcode  <= op_oeacc;

			assert FlagE = E report "error  failure" severity failure;

			wait for clk_cycle;
			opcode  <= nop;
		end procedure send_alu;

		variable A: std_logic_vector(7 downto 0);
		variable B: std_logic_vector(7 downto 0);
	begin

		-- addition
		wait for 2*clk_cycle;
		A := x"01"; B := x"02";
		send_alu(op_add, A, B, '0', '0', '0');
		assert databus = std_logic_vector(unsigned(A) + unsigned(B))
		report "addition failure" severity failure;

		-- addition zero
		wait for 2*clk_cycle;
		A := x"00"; B := x"00";
		send_alu(op_add, A, B, '1', '0', '0');
		assert databus = std_logic_vector(unsigned(A) + unsigned(B))
		report "addition failure" severity failure;

		-- addition nibble
		wait for 2*clk_cycle;
		A := x"0f"; B := x"01";
		send_alu(op_add, A, B, '0', '0', '1');
		assert databus = std_logic_vector(unsigned(A) + unsigned(B))
		report "addition failure" severity failure;

		-- addition carry and nibble
		wait for 2*clk_cycle;
		A := x"ff"; B := x"01";
		send_alu(op_add, A, B, '1', '1', '0');
		assert databus = std_logic_vector(unsigned(A) + unsigned(B))
		report "addition failure" severity failure;

		-- subtraction
		wait for 2*clk_cycle;
		A := x"05"; B := x"03";
		send_alu(op_sub, A, B, '0', '0', '0');
		assert databus = std_logic_vector(unsigned(A) - unsigned(B))
		report "subtraction failure" severity failure;

		-- subtraction zero
		wait for 2*clk_cycle;
		A := x"00"; B := x"00";
		send_alu(op_sub, A, B, '1', '0', '0');
		assert databus = std_logic_vector(unsigned(A) - unsigned(B))
		report "subtraction failure" severity failure;

		-- subtraction nibble
		wait for 2*clk_cycle;
		A := x"f0"; B := x"02";
		send_alu(op_sub, A, B, '0', '0', '1');
		assert databus = std_logic_vector(unsigned(A) - unsigned(B))
		report "subtraction failure" severity failure;

		-- subtraction carry and nibble
		wait for 2*clk_cycle;
		A := x"01"; B := x"05";
		send_alu(op_sub, A, B, '0', '1', '1');
		assert databus = std_logic_vector(unsigned(A) - unsigned(B))
		report "subtraction failure" severity failure;

		-- and
		wait for 2*clk_cycle;
		A := x"a2"; B := x"f0";
		send_alu(op_and, A, B, '0', '0', '0');
		assert databus = std_logic_vector(unsigned(A) and unsigned(B))
	       	report "and failure" & to_string(databus) severity failure;

		-- and zero
		wait for 2*clk_cycle;
		A := x"00"; B := x"00";
		send_alu(op_and, A, B, '1', '0', '0');
		assert databus = std_logic_vector(unsigned(A) and unsigned(B))
	       	report "and failure" & to_string(databus) severity failure;

		-- or
		wait for 2*clk_cycle;
		A := x"a2"; B := x"f0";
		send_alu(op_or, A, B, '0', '0', '0');
		assert databus = std_logic_vector(unsigned(A) or unsigned(B))
	       	report "or failure" & to_string(databus) severity failure;

		-- or zero
		wait for 2*clk_cycle;
		A := x"00"; B := x"00";
		send_alu(op_or, A, B, '1', '0', '0');
		assert databus = std_logic_vector(unsigned(A) or unsigned(B))
	       	report "or failure" & to_string(databus) severity failure;

		-- xor
		wait for 2*clk_cycle;
		A := x"a2"; B := x"f0";
		send_alu(op_xor, A, B, '0', '0', '0');
		assert databus = std_logic_vector(unsigned(A) xor unsigned(B))
	       	report "xor failure" & to_string(databus) severity failure;

		-- xor zero
		wait for 2*clk_cycle;
		A := x"00"; B := x"00";
		send_alu(op_xor, A, B, '1', '0', '0');
		assert databus = std_logic_vector(unsigned(A) xor unsigned(B))
	       	report "xor failure" & to_string(databus) severity failure;

		-- shift l
		wait for 2*clk_cycle;
		A := x"a2"; --          E
		send_alu(op_shiftl, A, '0');
		assert databus = std_logic_vector(unsigned(A) sll 1)
	       	report "sll failure" severity failure;

		-- shift r
		wait for 2*clk_cycle;
		A := x"a2";
		send_alu(op_shiftr, A, '0');
		assert databus = std_logic_vector(unsigned(A) srl 1)
		report "srl failure" severity failure;

		-- equality false
		wait for 2*clk_cycle;
		send_alu(op_cmpe, x"ae", x"af", '0', '0', '0');

		-- equality true
		wait for 2*clk_cycle;
		send_alu(op_cmpe, x"ae", x"ae", '1', '0', '0');

		-- greater than false
		wait for 2*clk_cycle;
		send_alu(op_cmpg, x"ae", x"af", '0', '0', '0');

		-- greater than false
		wait for 2*clk_cycle;
		send_alu(op_cmpg, x"ae", x"ae", '0', '0', '0');

		-- greater than true
		wait for 2*clk_cycle;
		send_alu(op_cmpg, x"af", x"ae", '1', '0', '0');

		-- less than false
		wait for 2*clk_cycle;
		send_alu(op_cmpl, x"af", x"ae", '0', '0', '0');

		-- less than false
		wait for 2*clk_cycle;
		send_alu(op_cmpl, x"ae", x"ae", '0', '0', '0');

		-- less than true
		wait for 2*clk_cycle;
		send_alu(op_cmpl, x"ae", x"af", '1', '0', '0');

		-- ascii2bin 48 -> 57
		for I in 48 to 57 loop
			wait for clk_cycle;
			send_alu(op_ascii2bin, std_logic_vector(to_unsigned(I, 8)), '0', '0');
			assert databus = std_logic_vector(to_unsigned(I - 48, 8))
			report "ascii2bin failure" severity failure;
		end loop;

		-- ascii2bin too small
		A := x"47";
		wait for clk_cycle;
		send_alu(op_ascii2bin, A, '1', '0');
		assert databus = x"ff"
		report "ascii2bin failure" severity failure;

		-- ascii2bin too big
		A := x"58";
		wait for clk_cycle;
		send_alu(op_ascii2bin, A, '1', '0');
		assert databus = x"ff"
		report "ascii2bin failure" severity failure;

		-- bin2ascii 0 -> 9
		for I in 0 to 9 loop
			wait for clk_cycle;
			send_alu(op_bin2ascii, std_logic_vector(to_unsigned(I, 8)), '0', '0');
			assert databus = std_logic_vector(to_unsigned(I + 48, 8))
			report "ascii2bin failure" severity failure;
		end loop;

		-- bin2ascii too big
		A := x"17";
		wait for clk_cycle;
		send_alu(op_bin2ascii, A, '1', '0');
		assert databus = x"ff"
		report "ascii2bin failure" severity failure;

		wait for 2*clk_cycle;
		report "Calling 'finish'"; finish;
	end process;
end testbench;
